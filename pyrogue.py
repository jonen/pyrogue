#!/usr/bin/env python

import curses

class window:
    scrn = None

def erase(y, x):
    window.scrn.addch(y, x, '#')
    return

def init():
    window.scrn = curses.initscr()
    window.scrn.clear()
    curses.noecho()
    curses.cbreak()
    window.scrn.keypad(1)
    curses.curs_set(0)

def restorescreen():
    curses.nocbreak()
    curses.echo()
    curses.endwin()

def game_loop(char, row, col, ch):
    window.scrn.addch(row, col, char)
    window.scrn.refresh()

    if ch == ord('q') or ch == ord('Q'):
        return

    while ch != ord('q') or ch != ord('Q'):
        ch = window.scrn.getch()
        if ch == ord('a') or ch == 260:
            erase(row, col)
            col = col - 1
            window.scrn.addch(row, col, char)
            window.scrn.refresh()
        elif ch == ord('d') or ch == 261:
            erase(row, col)
            col = col + 1
            window.scrn.addch(row, col, char)
            window.scrn.refresh()
        elif ch == ord('w') or ch == 259:
            erase(row, col)
            row = row - 1
            window.scrn.addch(row, col, char)
            window.scrn.refresh()
        elif ch == ord('s') or ch == 258:
            erase(row, col)
            row = row + 1
            window.scrn.addch(row, col, char)
            window.scrn.refresh()
        elif ch == ord('q') or ch == ord('Q'):
            return

my_row = 10
my_col = 10
main_char = '@'

init()

window.scrn.addstr(0, 0, "Welcome to pyrogue. A roguelike game in Python.\nPress any key to play or press q or Q to exit.\n")
my_ch = window.scrn.getch()

window.scrn.clear()
game_loop(main_char, my_row, my_col, my_ch)

restorescreen()
